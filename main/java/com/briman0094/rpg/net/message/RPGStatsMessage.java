package com.briman0094.rpg.net.message;

import io.netty.buffer.ByteBuf;

import java.io.ByteArrayInputStream;

import com.briman0094.rpg.rpgdata.RPGExtendedEntityProperties;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.IExtendedEntityProperties;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;

public class RPGStatsMessage implements IMessage
{
	public static final int DISCRIMINATOR = 1;
	
	public static class Handler implements IMessageHandler<RPGStatsMessage, IMessage>
	{
		
		@Override
		public IMessage onMessage(RPGStatsMessage message, MessageContext ctx)
		{
			if (ctx.side == Side.CLIENT && message._dataTag != null)
			{
				EntityPlayer player = Minecraft.getMinecraft().thePlayer;
				IExtendedEntityProperties iExt = player.getExtendedProperties(RPGExtendedEntityProperties.NAME);
				if (iExt != null && iExt instanceof RPGExtendedEntityProperties)
				{
					RPGExtendedEntityProperties ext = (RPGExtendedEntityProperties) iExt;
					ext.loadNBTData(message._dataTag);
				}
			}
			return null;
		}
		
	}

	protected NBTTagCompound	_dataTag	= null;
	
	public void setTagCompound(NBTTagCompound tag)
	{
		this._dataTag = tag;
	}
	
	public NBTTagCompound getTagCompound()
	{
		return this._dataTag;
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		ByteArrayInputStream inStream = new ByteArrayInputStream(buf.array());
		NBTTagCompound tag;
		tag = ByteBufUtils.readTag(buf);
		if (tag != null)
			this._dataTag = tag;
	}
	
	@Override
	public void toBytes(ByteBuf buf)
	{
		if (this._dataTag != null)
		{
			ByteBufUtils.writeTag(buf, this._dataTag);
		}
	}
	
}
