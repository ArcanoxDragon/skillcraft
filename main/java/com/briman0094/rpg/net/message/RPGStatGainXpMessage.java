package com.briman0094.rpg.net.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.IExtendedEntityProperties;

import com.briman0094.rpg.rpgdata.RPGExtendedEntityProperties;
import com.briman0094.rpg.stats.RPGStat;
import com.briman0094.rpg.stats.RPGStatRegistry;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;

public class RPGStatGainXpMessage implements IMessage
{
	public static final int DISCRIMINATOR = 2;
	
	public static class Handler implements IMessageHandler<RPGStatGainXpMessage, IMessage>
	{
		
		@Override
		public IMessage onMessage(RPGStatGainXpMessage message, MessageContext ctx)
		{
			if (ctx.side == Side.CLIENT)
			{
				EntityPlayer player = Minecraft.getMinecraft().thePlayer;
				IExtendedEntityProperties iExt = player.getExtendedProperties(RPGExtendedEntityProperties.NAME);
				if (iExt != null && iExt instanceof RPGExtendedEntityProperties)
				{
					RPGExtendedEntityProperties ext = (RPGExtendedEntityProperties) iExt;
					RPGStat stat = ext.getStat(RPGStatRegistry.getInstance().getStatTypeFromName(message.statName));
					if (stat != null)
					{
						stat.gainXP(message.gainedXP);
					}
				}
			}
			return null;
		}
		
	}
	
	public String	statName;
	public float	gainedXP;
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.statName = ByteBufUtils.readUTF8String(buf);
		this.gainedXP = buf.readFloat();
	}
	
	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.statName);
		buf.writeFloat(this.gainedXP);
	}
	
}
