package com.briman0094.rpg.net;

import com.briman0094.rpg.RPGMod;
import com.briman0094.rpg.net.message.RPGStatGainXpMessage;
import com.briman0094.rpg.net.message.RPGStatsMessage;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

public class RPGNetworkHandler
{
	private SimpleNetworkWrapper _netWrapper;
	
	public RPGNetworkHandler()
	{
		this._netWrapper = NetworkRegistry.INSTANCE.newSimpleChannel(RPGMod.MODID);
		
		this._netWrapper.registerMessage(RPGStatsMessage.Handler.class, RPGStatsMessage.class, RPGStatsMessage.DISCRIMINATOR, Side.CLIENT);
		this._netWrapper.registerMessage(RPGStatGainXpMessage.Handler.class, RPGStatGainXpMessage.class, RPGStatGainXpMessage.DISCRIMINATOR, Side.CLIENT);
	}
	
	public SimpleNetworkWrapper getNetWrapper()
	{
		return this._netWrapper;
	}
}
