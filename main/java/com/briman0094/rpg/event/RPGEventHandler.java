package com.briman0094.rpg.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.IExtendedEntityProperties;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;

import com.briman0094.rpg.RPGMod;
import com.briman0094.rpg.net.message.RPGStatsMessage;
import com.briman0094.rpg.rpgdata.RPGExtendedEntityProperties;
import com.briman0094.rpg.util.RPGUtil;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class RPGEventHandler
{
	
	@SubscribeEvent
	public void onEntityConstructed(EntityConstructing event)
	{
		if (event.entity instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) event.entity;
			player.registerExtendedProperties(RPGExtendedEntityProperties.NAME, new RPGExtendedEntityProperties());
		}
	}
	
	@SubscribeEvent
	public void onPlayerLoaded(PlayerLoggedInEvent event)
	{
		if (RPGUtil.isClient())
			return;
		
		if (!(event.player instanceof EntityPlayerMP))
			return;
		
		IExtendedEntityProperties iExt = event.player.getExtendedProperties(RPGExtendedEntityProperties.NAME);
		if (iExt != null && iExt instanceof RPGExtendedEntityProperties)
		{
			RPGExtendedEntityProperties ext = (RPGExtendedEntityProperties) iExt;
			NBTTagCompound tag = new NBTTagCompound();
			ext.saveNBTData(tag);
			RPGStatsMessage msg = new RPGStatsMessage();
			msg.setTagCompound(tag);
			RPGMod.PROXY.getNetHandler().getNetWrapper().sendTo(msg, (EntityPlayerMP) event.player);
		}
	}
}
