package com.briman0094.rpg.rpgdata;

import java.util.HashMap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

import com.briman0094.rpg.stats.RPGStat;
import com.briman0094.rpg.stats.RPGStatRegistry;
import com.briman0094.rpg.util.RPGLogger;
import com.briman0094.rpg.util.RPGUtil;

public class RPGExtendedEntityProperties implements IExtendedEntityProperties
{
	public static final String									NAME	= "skillCraftStats";
	
	private EntityPlayer										_player;
	private World												_world;
	private final HashMap<Class<? extends RPGStat>, RPGStat>	_stats;
	
	public RPGExtendedEntityProperties()
	{
		super();
		this._stats = new HashMap<Class<? extends RPGStat>, RPGStat>();
	}
	
	@Override
	public void saveNBTData(NBTTagCompound compound)
	{
		NBTTagCompound stats = new NBTTagCompound();
		for (RPGStat stat : this._stats.values())
		{
			stat.saveToNBT(stats);
		}
		compound.setTag(RPGExtendedEntityProperties.NAME, stats);
	}
	
	@Override
	public void loadNBTData(NBTTagCompound compound)
	{
		if (compound.hasKey(RPGExtendedEntityProperties.NAME))
		{
			NBTTagCompound stats = compound.getCompoundTag(RPGExtendedEntityProperties.NAME);
			
			for (Class statName : RPGStatRegistry.getInstance().getRegisteredStatTypes())
			{
				RPGStat newStat = RPGStatRegistry.getInstance().getNewStatInstance(statName, this._player);
				if (newStat != null)
				{
					this._stats.put(statName, newStat);
					newStat.readFromNBT(stats);
				}
			}
		}
	}
	
	public <T extends RPGStat> T getStat(Class<T> statType)
	{
		if (this._stats.containsKey(statType))
		{
			RPGStat newStat = this._stats.get(statType);
			if (statType.isInstance(newStat))
			{
				return statType.cast(newStat);
			}
			RPGLogger.log("Could not instantiate stat of type \"%s\"!", statType.getName());
			return null;
		}
		RPGLogger.log("No such stat with type \"%s\"", statType.getName());
		return null;
	}
	
	@Override
	public void init(Entity entity, World world)
	{
		if (entity instanceof EntityPlayer)
		{
			RPGLogger.log("Initializing extended properties %s for player %s...", toString(), entity.getEntityId());
			this._player = (EntityPlayer) entity;
			this._world = world;
			
			for (Class statName : RPGStatRegistry.getInstance().getRegisteredStatTypes())
			{
				RPGStat newStat = RPGStatRegistry.getInstance().getNewStatInstance(statName, this._player);
				if (newStat != null)
				{
					this._stats.put(statName, newStat);
				}
			}
			
		}
	}
	
}
