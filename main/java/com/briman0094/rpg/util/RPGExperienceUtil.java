package com.briman0094.rpg.util;

public class RPGExperienceUtil
{
	
	
	public static double getConstantA(int numLevels, double maxLevelTotalXp, double secondLevelXp)
	{
		double M = maxLevelTotalXp;
		double N = secondLevelXp;
		double Q = numLevels;
		
		double A = Math.pow((Math.exp(Q) * Math.pow(N,  Q)) / M, 1.0 / (Q - 1.0));
		return A;
	}
	
	public static double getConstantB(double constantA, double secondLevelXp)
	{
		double N = secondLevelXp;
		double A = constantA;
		
		double B = Math.log(N / A) + 1.0;
		return B;
	}
	
	public static double getTotalExperienceForLevel(double level, double secondLevelXp, double maxLevelTotalXp, int numLevels)
	{
		double M = maxLevelTotalXp;
		double N = secondLevelXp;
		int Q = numLevels;
		
		double A = getConstantA(Q, M, N);
		double B = getConstantB(A, N);
		
		return A * Math.exp(B * level);
	}
	
	public static double getLevelFromExperience(double xp, double secondLevelXp, double maxLevelTotalXp, int numLevels)
	{
		double M = maxLevelTotalXp;
		double N = secondLevelXp;
		int Q = numLevels;
		
		double A = getConstantA(Q, M, N);
		double B = getConstantB(A, N);
		
		return Math.log(xp / A) / B;
	}
	
	/*public static double getLevelFromExperience(double xp, double minLevelExp, double maxLevelExp, int numLevels)
	{
		if (xp <= Double.MIN_VALUE)
			return 0.0;
		
		double Q = minLevelExp;
		double R = maxLevelExp;
		double S = numLevels;
		
		double B = Math.log(R / Q) / (S - 1.0);
		double A = Q / Math.exp(B - 1.0);
		
		double level = Math.max(Math.log(xp / A) / B, 0.0);
		
		return level + 1;
	}*/
	
	public static double getExperienceToNextLevel(int curLevel, double minLevelXp, double maxLevelTotalXp, int numLevels)
	{
		double thisLevelXp = getTotalExperienceForLevel(curLevel, minLevelXp, maxLevelTotalXp, numLevels);
		double nextLevelXp = getTotalExperienceForLevel(curLevel + 1, minLevelXp, maxLevelTotalXp, numLevels);
		
		return nextLevelXp - thisLevelXp;
	}
	
	/*public static double getExperienceToNextLevel(int curLevel, double minLevelExp, double maxLevelExp, int numLevels)
	{
		double Q = minLevelExp;
		double R = maxLevelExp;
		double S = numLevels;
		
		double B = Math.log(R / Q) / (S - 1.0);
		double A = Q / (Math.exp(B) - 1.0);
		
		double lastXP = A * Math.exp(B * (curLevel - 1));
		double nextXP = A * Math.exp(B * curLevel);
		
		return nextXP - lastXP;
	}*/
}
