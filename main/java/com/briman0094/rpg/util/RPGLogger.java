package com.briman0094.rpg.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.briman0094.rpg.RPGMod;

public class RPGLogger
{
	private static Logger	_logger;
	
	static
	{
		RPGLogger._logger = LogManager.getFormatterLogger(RPGMod.MODNAME);
	}
	
	public static void log(String msg, Object... args)
	{
		RPGLogger._logger.log(Level.INFO, msg, args);
	}
}
