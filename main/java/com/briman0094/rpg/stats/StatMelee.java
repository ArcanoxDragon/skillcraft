package com.briman0094.rpg.stats;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EntityDamageSource;
import net.minecraftforge.common.IExtendedEntityProperties;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

import com.briman0094.rpg.rpgdata.RPGExtendedEntityProperties;
import com.briman0094.rpg.util.RPGLogger;
import com.briman0094.rpg.util.RPGUtil;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class StatMelee extends RPGStat
{
	public static final String	STAT_NAME	= "Melee Attack";
	
	public StatMelee(EntityPlayer statOwner)
	{
		super(statOwner);
	}
	
	private static StatEventHandler	_eventHandler;
	
	public static class StatEventHandler
	{
		private StatEventHandler()
		{
		}
		
		@SubscribeEvent
		public void onEntityDamaged(LivingHurtEvent event)
		{
			if (RPGUtil.isClient())
				return;
			
			if (event.source instanceof EntityDamageSource)
			{
				EntityDamageSource eSource = (EntityDamageSource) event.source;
				Entity sourceEntity = eSource.getSourceOfDamage();
				if (sourceEntity instanceof EntityPlayer)
				{
					
					EntityPlayer player = (EntityPlayer) sourceEntity;
					IExtendedEntityProperties ext = player.getExtendedProperties(RPGExtendedEntityProperties.NAME);
					if (ext != null && ext instanceof RPGExtendedEntityProperties)
					{
						RPGExtendedEntityProperties rpgExt = (RPGExtendedEntityProperties) ext;
						StatMelee stat = rpgExt.getStat(StatMelee.class);
						if (stat != null)
							stat.gainXP(10.0f);
						player.addExperience(1);
					}
				}
			}
		}
	}
	
	static
	{
		StatMelee._eventHandler = new StatEventHandler();
		MinecraftForge.EVENT_BUS.register(StatMelee._eventHandler);
	}
	
	@Override
	protected void getCustomDataFromNBT(NBTTagCompound tag)
	{
		RPGLogger.log("Melee stat loaded with %04.2f XP currently!", this.getCurrentXP());
	}
	
	@Override
	public String getStatName()
	{
		return STAT_NAME;
	}
	
}
