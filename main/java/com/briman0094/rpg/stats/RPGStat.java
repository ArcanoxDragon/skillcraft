package com.briman0094.rpg.stats;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

import com.briman0094.rpg.RPGCommonProxy;
import com.briman0094.rpg.RPGMod;
import com.briman0094.rpg.net.message.RPGStatGainXpMessage;
import com.briman0094.rpg.util.RPGExperienceUtil;
import com.briman0094.rpg.util.RPGUtil;

public abstract class RPGStat
{
	public static final int	NUM_LEVELS	= 100;
	
	protected float			_curXP;
	protected EntityPlayer _player;
	
	public RPGStat(EntityPlayer statOwner)
	{
		this._player = statOwner;
	}
	
	public int getCurrentLevel()
	{
		return (int) Math.floor(RPGExperienceUtil.getLevelFromExperience(this._curXP, getFirstLevelExperience(), getLastLevelExperience(), NUM_LEVELS));
	}
	
	public float getXPToNextLevel()
	{
		return (float) RPGExperienceUtil.getExperienceToNextLevel(getCurrentLevel(), getFirstLevelExperience(), getLastLevelExperience(), NUM_LEVELS);
	}
	
	public float getCurrentXP()
	{
		return this._curXP;
	}
	
	protected void setCurrentXP(float xp)
	{
		this._curXP = xp;
	}
	
	public void gainXP(float xp)
	{
		if (RPGUtil.isServer() && this._player instanceof EntityPlayerMP)
		{
			RPGStatGainXpMessage msg = new RPGStatGainXpMessage();
			msg.statName = this.getStatName();
			msg.gainedXP = xp;
			RPGMod.PROXY.getNetHandler().getNetWrapper().sendTo(msg, (EntityPlayerMP) this._player);
		}
		
		this._curXP += xp;
	}
	
	public abstract String getStatName();
	
	/**
	 * Returns the amount of experience required for the 2nd level (starts at 1,
	 * not 0) Default is 20
	 * 
	 * @return
	 */
	public int getFirstLevelExperience()
	{
		return 20;
	}
	
	/**
	 * Returns the amount of experience required for the 100th level Default is
	 * 1,000,000
	 * 
	 * @return
	 */
	public int getLastLevelExperience()
	{
		return 1000000;
	}
	
	public final void saveToNBT(NBTTagCompound tag)
	{
		NBTTagCompound myTag = new NBTTagCompound();
		myTag.setFloat("xp", getCurrentXP());
		addCustomDataToNBT(myTag);
		tag.setTag("stat" + getStatName(), myTag);
	}
	
	protected void addCustomDataToNBT(NBTTagCompound tag)
	{
	}
	
	public final void readFromNBT(NBTTagCompound tag)
	{
		if (tag.hasKey("stat" + getStatName()))
		{
			NBTTagCompound statTag = tag.getCompoundTag("stat" + getStatName());
			setCurrentXP(statTag.getFloat("xp"));
			getCustomDataFromNBT(statTag);
		}
	}
	
	protected void getCustomDataFromNBT(NBTTagCompound tag)
	{
	}
}
