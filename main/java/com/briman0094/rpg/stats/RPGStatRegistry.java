package com.briman0094.rpg.stats;

import java.util.ArrayList;
import java.util.HashMap;

import net.minecraft.entity.player.EntityPlayer;

import com.briman0094.rpg.util.RPGLogger;

public class RPGStatRegistry
{
	/* Static */
	private static RPGStatRegistry	_instance;
	
	static
	{
		RPGStatRegistry._instance = new RPGStatRegistry();
	}
	
	public static RPGStatRegistry getInstance()
	{
		return RPGStatRegistry._instance;
	}
	
	/* End Static */
	
	private final HashMap<String, Class<? extends RPGStat>>	_statTypesFromName;
	private final HashMap<Class<? extends RPGStat>, String>	_statNamesFromType;
	
	public RPGStatRegistry()
	{
		this._statTypesFromName = new HashMap<String, Class<? extends RPGStat>>();
		this._statNamesFromType = new HashMap<Class<? extends RPGStat>, String>();
	}
	
	public void registerStatType(String name, Class<? extends RPGStat> type)
	{
		if (this._statTypesFromName.containsKey(name) || this._statNamesFromType.containsKey(type))
		{
			RPGLogger.log("A mod tried to register a duplicate stat for name \"%s\" with class \"%\"!\n\tThis stat will not be registered.", name, type.getName());
			return;
		}
		
		this._statTypesFromName.put(name, type);
		this._statNamesFromType.put(type, name);
	}
	
	public ArrayList<String> getRegisteredStatNames()
	{
		return new ArrayList<String>(this._statTypesFromName.keySet());
	}
	
	public ArrayList<Class<? extends RPGStat>> getRegisteredStatTypes()
	{
		return new ArrayList<Class<? extends RPGStat>>(this._statNamesFromType.keySet());
	}
	
	public Class<? extends RPGStat> getStatTypeFromName(String name)
	{
		if (!this._statTypesFromName.containsKey(name))
			return null;
		return this._statTypesFromName.get(name);
	}
	
	public String getStatNameFromType(Class<? extends RPGStat> type)
	{
		if (!this._statNamesFromType.containsKey(type))
			return null;
		return this._statNamesFromType.get(type);
	}
	
	public <T extends RPGStat> T getNewStatInstance(Class<T> type, EntityPlayer owner)
	{
		if (!this._statNamesFromType.containsKey(type))
			return null;
		String name = this._statNamesFromType.get(type);
		try
		{
			RPGStat stat = (RPGStat) type.getConstructors()[0].newInstance(owner);
			if (stat != null && type.isInstance(stat))
				return type.cast(stat);
		}
		catch (Exception ex)
		{
			RPGLogger.log("An exception occurred while trying to initialize a new instance of stat \"%s\":", name);
			ex.printStackTrace();
		}
		RPGLogger.log("Stat with name \"%s\" could not be initialized. This stat will not be tracked or saved.", name);
		return null;
	}
	
	public RPGStat getNewStatInstance(String name, EntityPlayer owner)
	{
		if (!this._statTypesFromName.containsKey(name))
			return null;
		return getNewStatInstance(this._statTypesFromName.get(name), owner);
	}
}
