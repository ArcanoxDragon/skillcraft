package com.briman0094.rpg;

import net.minecraftforge.common.MinecraftForge;

import com.briman0094.rpg.event.RPGEventHandler;
import com.briman0094.rpg.net.RPGNetworkHandler;
import com.briman0094.rpg.stats.RPGStatRegistry;
import com.briman0094.rpg.stats.StatMelee;
import com.briman0094.rpg.util.RPGLogger;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLEvent;

public class RPGCommonProxy
{
	private RPGNetworkHandler _netHandler;
	
	public void preInit()
	{
		registerDefaultStats();
		this._netHandler = new RPGNetworkHandler();
	}
	
	public void init()
	{
		RPGEventHandler eventHandler = new RPGEventHandler();
		MinecraftForge.EVENT_BUS.register(eventHandler);
		FMLCommonHandler.instance().bus().register(eventHandler);
	}
	
	public RPGNetworkHandler getNetHandler()
	{
		return this._netHandler;
	}
	
	private void registerDefaultStats()
	{
		RPGLogger.log("Registering default stats for SkillCraft...");
		
		RPGStatRegistry.getInstance().registerStatType(StatMelee.STAT_NAME, StatMelee.class);
	}
}
