package com.briman0094.rpg;

import net.minecraftforge.common.MinecraftForge;

import com.briman0094.rpg.event.RPGEventHandler;
import com.briman0094.rpg.stats.RPGStatRegistry;
import com.briman0094.rpg.stats.StatMelee;
import com.briman0094.rpg.util.RPGLogger;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(name = RPGMod.MODNAME, modid = RPGMod.MODID, version = RPGMod.VERSION)
public class RPGMod
{
	public static final String	MODNAME	= "SkillCraft";
	public static final String	MODID	= "skillcraft";
	public static final String	VERSION	= "1.0";
	
	@SidedProxy(clientSide = "com.briman0094.rpg.client.RPGClientProxy", serverSide = "com.briman0094.rpg.RPGCommonProxy")
	public static RPGCommonProxy PROXY;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		RPGLogger.log("%s is initializing...", RPGMod.MODNAME);
		
		PROXY.preInit();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		RPGLogger.log("Registering event handlers for %s...", RPGMod.MODNAME);
		
		PROXY.init();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		RPGLogger.log("%s initialized successfully!", RPGMod.MODNAME);
	}
}
