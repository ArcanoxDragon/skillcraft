package com.briman0094.rpg.client.util;

import net.minecraft.client.renderer.Tessellator;

public class RPGClientGuiUtil
{
	
	public static void drawTexturedModalRect(int x, int y, int width, int height, int u, int v, int texWidth, int texHeight)
	{
		float uScale = 1.0f / texWidth;
		float vScale = 1.0f / texHeight;
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV(x, y + height, 0, u * uScale, (v + height) * vScale);
		tessellator.addVertexWithUV(x + width, y + height, 0, (u + width) * uScale, (v + height) * vScale);
		tessellator.addVertexWithUV(x + width, y, 0, (u + width) * uScale, v * vScale);
		tessellator.addVertexWithUV(x, y, 0, u * uScale, v * vScale);
		tessellator.draw();
	}
	
}
