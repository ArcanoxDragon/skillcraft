package com.briman0094.rpg.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.IExtendedEntityProperties;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import com.briman0094.rpg.RPGMod;
import com.briman0094.rpg.client.RPGClientProxy;
import com.briman0094.rpg.client.util.RPGClientGuiUtil;
import com.briman0094.rpg.rpgdata.RPGExtendedEntityProperties;
import com.briman0094.rpg.stats.RPGStat;
import com.briman0094.rpg.stats.RPGStatRegistry;

public class GuiRPGStats extends GuiScreen
{
	public static final ResourceLocation	texBackground		= new ResourceLocation(RPGMod.MODID, "textures/gui/skillMenu.png");
	private static final int				GUI_TEX_WIDTH		= 512;
	private static final int				GUI_TEX_HEIGHT		= 512;
	private static final int				STATS_PER_PAGE		= 10;
	private static final int				BTN_ID_LAST_PAGE	= 0;
	private static final int				BTN_ID_NEXT_PAGE	= 1;
	private static final int				STAT_START_Y		= 38;
	
	private int								_x, _y;
	private int								_width, _height;
	private ArrayList<RPGStat>				_stats;
	private int								_curPage			= 0;
	private int								_numPages;
	private GuiButtonStatPage				_btnLastPage;
	private GuiButtonStatPage				_btnNextPage;
	
	public GuiRPGStats(EntityPlayer player)
	{
		this._stats = new ArrayList<RPGStat>();
		if (player != null)
		{
			IExtendedEntityProperties ext = player.getExtendedProperties(RPGExtendedEntityProperties.NAME);
			if (ext != null && ext instanceof RPGExtendedEntityProperties)
			{
				RPGExtendedEntityProperties rpgExt = (RPGExtendedEntityProperties) ext;
				int i = 0;
				for (Class<? extends RPGStat> statType : RPGStatRegistry.getInstance().getRegisteredStatTypes())
				{
					RPGStat stat = rpgExt.getStat(statType);
					if (stat != null)
						this._stats.add(stat);
				}
			}
		}
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		this._width = 320;
		this._height = 240;
		
		this._x = (this.width - this._width) / 2;
		this._y = (this.height - this._height) / 2;
		
		this._numPages = Math.max((int) Math.ceil((double) this._stats.size() / STATS_PER_PAGE), 1);
		
		this._btnLastPage = new GuiButtonStatPage(BTN_ID_LAST_PAGE, this._x + 16, this._y + this._height - 30);
		this._btnNextPage = new GuiButtonStatPage(BTN_ID_NEXT_PAGE, this._x + this._width - 40, this._y + this._height - 30);
		this._btnLastPage.setArrowFacing(true);
		this._btnNextPage.setVisible(this._curPage + 1 < this._numPages);
		this._btnLastPage.setVisible(this._curPage > 0);
		
		this.buttonList.add(this._btnLastPage);
		this.buttonList.add(this._btnNextPage);
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		
		this._btnNextPage.setVisible(this._curPage + 1 < this._numPages);
		this._btnLastPage.setVisible(this._curPage > 0);
	}
	
	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (button.id == BTN_ID_LAST_PAGE)
		{
			if (this._btnLastPage.isVisible())
				if (this._curPage > 0)
					this._curPage--;
		}
		else if (button.id == BTN_ID_NEXT_PAGE)
		{
			
			if (this._btnNextPage.isVisible())
				if (this._curPage + 1 < this._numPages)
					this._curPage++;
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTick)
	{
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseY, partialTick);
		
		this.mc.getTextureManager().bindTexture(GuiRPGStats.texBackground);
		RPGClientGuiUtil.drawTexturedModalRect(this._x, this._y, this._width, this._height, 0, 0, GuiRPGStats.GUI_TEX_WIDTH, GuiRPGStats.GUI_TEX_HEIGHT);
		
		drawCenteredText("skillCraft.gui.statsMenu", this._x + (this._width / 2), this._y + 16, 0xDD11DD, 1.5);
		drawCenteredText(String.format("%d / %d", this._curPage + 1, this._numPages), this._x + (this._width / 2), this._y + (this._height - 24), 0x000000, 1.0);
		
		int statStart = this._curPage * STATS_PER_PAGE;
		int statEnd = statStart + Math.min(STATS_PER_PAGE, this._stats.size() - statStart);
		for (int s = statStart; s < statEnd; s++)
		{
			int statY = (s % STATS_PER_PAGE) * 32;
			RPGStat stat = this._stats.get(s);
			this.fontRendererObj.drawString(String.format("[%3d] %s: %d / %d", stat.getCurrentLevel(), stat.getStatName(), (int) stat.getCurrentXP(), (int) stat.getXPToNextLevel()), this._x + 20, this._y + STAT_START_Y + statY, 0x000000, false);
		}
	}
	
	private void drawCenteredText(String text, int x, int y, int color, double scale)
	{
		String translatedText = I18n.format(text);
		int textX = x - (int) (this.fontRendererObj.getStringWidth(translatedText) / 2 * scale);
		GL11.glPushMatrix();
		GL11.glScaled(scale, scale, 1.0);
		this.fontRendererObj.drawString(translatedText, (int) ((textX + 1) / scale), (int) ((y + 1) / scale), 0xCCCCCC);
		this.fontRendererObj.drawString(translatedText, (int) (textX / scale), (int) (y / scale), color);
		GL11.glPopMatrix();
	}
	
	@Override
	protected void keyTyped(char keyChar, int keyCode)
	{
		if (keyCode == this.mc.gameSettings.keyBindInventory.getKeyCode() || keyCode == RPGClientProxy.keyBindingStats.getKeyCode() || keyCode == Keyboard.KEY_ESCAPE)
		{
			this.mc.thePlayer.closeScreen();
		}
	}
}
