package com.briman0094.rpg.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;

import org.lwjgl.opengl.GL11;

import com.briman0094.rpg.util.RPGLogger;

public class GuiButtonStatPage extends GuiButton
{
	/* UV coordinates on texture */
	private static final double	UV_RIGHT_ARROW_X	= 0.625;
	private static final double	UV_LEFT_ARROW_X		= 0.6875;
	private static final double	UV_UNSELECTED_Y		= 0.0;
	private static final double	UV_SELECTED_Y		= 0.0625;
	private static final double	UV_ARROW_W			= 0.0625;
	private static final double	UV_ARROW_H			= 0.046875;
	
	private static final int	BUTTON_WIDTH		= 32;
	private static final int	BUTTON_HEIGHT		= 24;
	
	private boolean				_leftFacing;
	private boolean				_visible;
	
	public GuiButtonStatPage(int id, int x, int y)
	{
		super(id, x, y, BUTTON_WIDTH, BUTTON_HEIGHT, "");
		this._leftFacing = false;
		this._visible = true;
	}
	
	/**
	 * Sets the direction the arrow on the button faces
	 * 
	 * @param facing
	 *            true for left, false for right
	 */
	public void setArrowFacing(boolean facing)
	{
		this._leftFacing = facing;
	}
	
	public void setVisible(boolean visible)
	{
		this._visible = visible;
		this.enabled = visible;
	}
	
	public boolean isVisible()
	{
		return this._visible;
	}
	
	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
		if (!this._visible)
			return;
		
		GL11.glColor4d(1.0, 1.0, 1.0, 1.0);
		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, 1, 0);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		boolean selected = (mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX <= this.xPosition + this.width && mouseY <= this.yPosition + this.height);
		
		double uL = this._leftFacing ? UV_LEFT_ARROW_X : UV_RIGHT_ARROW_X;
		double uR = uL + UV_ARROW_W;
		double vT = selected ? UV_SELECTED_Y : UV_UNSELECTED_Y;
		double vB = vT + UV_ARROW_H;
		int x = this.xPosition;
		int x2 = x + this.width;
		int y = this.yPosition;
		int y2 = y + this.height;
		
		mc.getTextureManager().bindTexture(GuiRPGStats.texBackground);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.addVertexWithUV(x, y, 1, uL, vT);
		tessellator.addVertexWithUV(x, y2, 1, uL, vB);
		tessellator.addVertexWithUV(x2, y2, 1, uR, vB);
		tessellator.addVertexWithUV(x2, y, 1, uR, vT);
		tessellator.draw();
	}
	
}
