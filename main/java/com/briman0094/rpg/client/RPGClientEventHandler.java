package com.briman0094.rpg.client;

import net.minecraft.client.Minecraft;

import com.briman0094.rpg.client.gui.GuiRPGStats;
import com.briman0094.rpg.util.RPGLogger;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;

public class RPGClientEventHandler
{
	@SubscribeEvent
	public void onKeyInput(KeyInputEvent event)
	{
		if (RPGClientProxy.keyBindingStats.isPressed() && !(Minecraft.getMinecraft().currentScreen instanceof GuiRPGStats))
		{
			FMLClientHandler.instance().showGuiScreen(new GuiRPGStats(Minecraft.getMinecraft().thePlayer));
		}
	}
}
