package com.briman0094.rpg.client;

import net.minecraft.client.settings.KeyBinding;

import org.lwjgl.input.Keyboard;

import com.briman0094.rpg.RPGCommonProxy;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;

public class RPGClientProxy extends RPGCommonProxy
{
	public static KeyBinding	keyBindingStats;
	
	public RPGClientProxy()
	{
		RPGClientProxy.keyBindingStats = new KeyBinding("skillCraft.key.openStats", Keyboard.KEY_G, "key.categories.inventory");
	}
	
	@Override
	public void init()
	{
		super.init();
		
		FMLCommonHandler.instance().bus().register(new RPGClientEventHandler());
		ClientRegistry.registerKeyBinding(RPGClientProxy.keyBindingStats);
	}
}
